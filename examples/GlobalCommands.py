import tkinter as tk                # python 3
from tkinter import *
from tkinter import font
import paho.mqtt.client as mqtt
import inspect


root = Tk()

publicTopic = "Test";

def on_connect(mqttc, obj, flags, rc):
    print("connected with rc code: " + str(rc))


def on_message(mqttc, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))
    pass


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    print(string)



mqttc = mqtt.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe

#mqttc.username_pw_set("nlmafkti", password="iiqtq4waxTq3")
# Uncomment to enable debug messages
# mqttc.on_log = on_log

mqttc.username_pw_set("nlmafkti","iiqtq4waxTq3")
mqttc.connect("34.200.51.91", 13022, 60)
mqttc.loop_start()


"""class FullScreenApp(object):
    def __init__(self, master, **kwargs):
        self.master=master
        pad=3
        self._geom='200x200+0+0'
        master.geometry("{0}x{1}+0+0".format(
            master.winfo_screenwidth()-pad, master.winfo_screenheight()-pad))
        master.bind('<Escape>',self.toggle_geom)
    def toggle_geom(self,event):
        geom=self.master.winfo_geometry()
        print(geom,self._geom)
        self.master.geometry(self._geom)
        self._geom=geom

root=tk.Tk()
app=FullScreenApp(root)"""


root.mainloop()

print("tuple")
(rc, mid) = mqttc.publish("tuple", "bar", qos=2)
print("class")
infot = mqttc.publish("class", "bar", qos=2)

infot.wait_for_publish()

def LampsOn():
    mqttc.publish(publicTopic, currentFunctionName())

def LampsOff():
    mqttc.publish(publicTopic, currentFunctionName())

def AllRollersUp():
    mqttc.publish(publicTopic,  currentFunctionName())

def AllRollersDown():
    mqttc.publish(publicTopic,  currentFunctionName())

def Away():
    mqttc.publish(publicTopic,  currentFunctionName())

def AllAcOn():
    mqttc.publish(publicTopic, currentFunctionName())

def AllAcOff():
    mqttc.publish(publicTopic, currentFunctionName())

def currentFunctionName():
    return inspect.stack()[1][3]


class GlobalCommands(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent )
        self.controller = controller

        Grid.rowconfigure(self, 0, weight=1)
        Grid.columnconfigure(self, 0, weight=1)

        # Create & Configure frame
        frame = Frame(self)
        frame.grid(row=0, column=0, sticky=N + S + E + W)

        # Create a 3x3 (rows x columns) grid of buttons inside the frame

        for row_index in range(3):
            Grid.rowconfigure(frame, row_index, weight=1)
            for col_index in range(2):
                Grid.columnconfigure(frame, col_index, weight=1)

        btnfont1 = font.Font(family='Courier', size=12, weight='bold')
        btnfont2 = font.Font(family='Courier', size=14, weight='normal')
        btnfont3 = font.Font(family='Courier', size=14, weight='normal')

        # All Lamps On
        btn11 = Button(frame, text="ALL LAMPS ON", font=btnfont1, fg='OrangeRed2', bg='gray24', command = LampsOn)
        btn11.grid(row=0, column=0, sticky=N + S + E + W)

        # All Lamps Off
        btn12 = Button(frame, text="ALL LAMPS OFF", font=btnfont1, fg='OrangeRed2', bg='gray24', command = LampsOff)
        btn12.grid(row=0, column=1, sticky=N + S + E + W)

        # All Lamps Off
        btn21 = Button(frame, text = "ALL ROLLERS UP", font=btnfont1, fg='OrangeRed2', bg='gray24', command = AllRollersUp)
        btn21.grid(row=1, column=0, sticky=N + S + E + W)

        # All Lamps Off
        btn22 = Button(frame, text="ALL ROLLERS DOWN", font=btnfont1, fg='OrangeRed2', bg='gray24', command = AllRollersDown)
        btn22.grid(row=1, column=1, sticky=N + S + E + W)

        # All Lamps Off
        btn31 = Button(frame, text="ALL AC ON", font=btnfont1, fg='OrangeRed2', bg='gray24', command = AllAcOn)
        btn31.grid(row=2, column=0, sticky=N + S + E + W)

        # All Lamps Off
        btn32 = Button(frame, text="ALL AC OFF", font=btnfont1, fg='OrangeRed2', bg='gray24', command = AllAcOff)
        btn32.grid(row=2, column=1, sticky=N + S + E + W)

        Grid.rowconfigure(frame, 4, weight=1)

        btn_main = Button(frame, text="MAIN MENU", font=btnfont1, fg='OrangeRed2', bg='gray30',
                          command=lambda: controller.show_frame("MainMenu"))

        btn_main.grid(row=4, column=0, sticky=N + S + E + W, columnspan=2)
