import tkinter as tk                # python 3
from tkinter import font as tkfont # python 3
from tkinter import *
import paho.mqtt.client as mqtt



def on_connect(mqttc, obj, flags, rc):
    print("connected with rc code: " + str(rc))


def on_message(mqttc, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))
    pass


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    print(string)


# If you want to use a specific client id, used
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.

mqttc = mqtt.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe

#mqttc.username_pw_set("nlmafkti", password="iiqtq4waxTq3")
# Uncomment to enable debug messages
# mqttc.on_log = on_log

mqttc.username_pw_set("nlmafkti","iiqtq4waxTq3")
mqttc.connect("34.200.51.91", 13022, 60)
mqttc.loop_start()



def AllLampsOn():

    mqttc.publish("Test", "lamps_On")

def AllLampsOff():

    mqttc.publish("Test", "lamps_Off")

def AllRollersUp():

    mqttc.publish("Test", "Rollers_Up")

def AllRollersDown():

    mqttc.publish("Test", "Rollers_Down")

def Away():

    mqttc.publish("Test", "Away")

# print("tuple")
# (rc, mid) = mqttc.publish("tuple", "bar", qos=2)
# print("class")
# infot = mqttc.publish("class", "bar", qos=2)

# nfot.wait_for_publish()



class SampleApp(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        self.title_font = tkfont.Font(family='Helvetica', size=18, weight="bold", slant="italic")

        # the container is where we'll stack a bunch of frames
        # on top of each other, then the one we want visible
        # will be raised above the others
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (Scenarios, MainMenu, GlobalCommands):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

            # put all of the pages in the same location;
            # the one on the top of the stacking order
            # will be the one that is visible.
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("Scenarios")

    def show_frame(self, page_name):

        '''Show a frame for the given page name'''
        frame = self.frames[page_name]
        frame.tkraise()

    def setFullScreen(self):
        SW = self.winfo_screenwidth()
        SH = self.winfo_screenheight()
        self.geometry("%dx%d+0+0" % (SW, SH))


class Scenarios(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        Grid.rowconfigure(self, 0, weight=1)
        Grid.columnconfigure(self, 0, weight=1)

        # Create & Configure frame
        frame = Frame(self)
        frame.grid(row=0, column=0, sticky=N + S + E + W)

        # Create a 3x3 (rows x columns) grid of buttons inside the frame

        for row_index in range(3):
            Grid.rowconfigure(frame, row_index, weight=2)
            for col_index in range(2):
                Grid.columnconfigure(frame, col_index, weight=2)

        btnfont1 = tkfont.Font(family='Courier', size=12, weight='bold')
        btnfont2 = font.Font(family='Courier', size=14, weight='normal')
        btnfont3 = font.Font(family='Calibri', size=18, weight='bold')

        # Label

        Grid.rowconfigure(frame, 0, weight=1)
        w = Label(frame, text="SCENARIOS", font=btnfont3, fg='blue', bg='gray30')

        w.grid(row=0, column=0, sticky=N + S + E + W, columnspan=2)


        # GOOD MORNING
        btn11 = Button(frame, text="GOOD MORNING", font=btnfont1, fg='OrangeRed2', bg='gray24')
        btn11.grid(row=1, column=0, sticky=N + S + E + W)

        # CHILL EVENING
        btn12 = Button(frame, text="CHILL EVENING", font=btnfont1, fg='OrangeRed2', bg='gray24')
        btn12.grid(row=1, column=1, sticky=N + S + E + W)

        # AWAY
        btn21 = Button(frame, text="AWAY", font=btnfont1, fg='OrangeRed2', bg='gray24')
        btn21.grid(row=2, column=0, sticky=N + S + E + W)

        # ECO
        btn22 = Button(frame, text="ECO", font=btnfont1, fg='OrangeRed2', bg='gray24')
        btn22.grid(row=2, column=1, sticky=N + S + E + W)

        # Main
        Grid.rowconfigure(frame , 3, weight=2)
        btn_main = Button(frame, text="MAIN MENU", font=btnfont1, fg='OrangeRed2', bg='gray30',
                          command = lambda: controller.show_frame("MainMenu"))

        btn_main.grid(row=3, column=0, sticky=N + S + E + W, columnspan=2)



class MainMenu(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        Grid.rowconfigure(self, 0, weight=1)
        Grid.columnconfigure(self, 0, weight=1)

        # Create & Configure frame
        frame = Frame(self)
        frame.grid(row=0, column=0, sticky=N + S + E + W)

        # Create (rows x columns) grid of buttons inside the frame

        for row_index in range(4):
            Grid.rowconfigure(frame, row_index, weight=2)
            Grid.columnconfigure(frame, 0,  weight = 2)


        btnfont1 = font.Font(family='Courier', size=12, weight='bold')
        btnfont2 = font.Font(family='Courier', size=14, weight='normal')
        btnfont3 = font.Font(family='Calibri', size=18, weight='bold')


        # Label

        Grid.rowconfigure(frame, 0, weight=1)
        w = Label(frame, text="MAIN MENU", font=btnfont3, fg='red', bg='gray30')
        w.grid(row=0, column=0, sticky=N + S + E + W, columnspan=2)


        # GlobalCommands
        btn11 = Button(frame, text="GLOBAL COMMANDS", font=btnfont1, fg='OrangeRed2', bg='gray24',
                       command= lambda : controller.show_frame("GlobalCommands"))
        btn11.grid(row=1, column=0, sticky=N + S + E + W)

        # SCENARIOS
        btn12 = Button(frame, text="SCENARIOS", font=btnfont1, fg='OrangeRed2', bg='gray24',
                       command=lambda : controller.show_frame("Scenarios"))
        btn12.grid(row=2, column=0, sticky=N + S + E + W)

        # ROOMS
        btn21 = Button(frame, text="ROOMS", font=btnfont1, fg='OrangeRed2', bg='gray24',
                       command= lambda : controller.show_frame("Rooms"))
        btn21.grid(row=3, column=0, sticky=N + S + E + W)


class GlobalCommands(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent )
        self.controller = controller

        Grid.rowconfigure(self, 0, weight=1)
        Grid.columnconfigure(self, 0, weight=1)

        # Create & Configure frame
        frame = Frame(self)
        frame.grid(row=0, column=0, sticky=N + S + E + W)

        # Create a 3x3 (rows x columns) grid of buttons inside the frame

        for row_index in range(5):
            Grid.rowconfigure(frame, row_index, weight=1)
            for col_index in range(2):
                Grid.columnconfigure(frame, col_index, weight=1)


        btnfont1 = font.Font(family='Courier', size=12, weight='bold')
        btnfont2 = font.Font(family='Courier', size=14, weight='bold')
        btnfont3 = font.Font(family='Calibri', size=18, weight='bold')


        # Label

        Grid.rowconfigure(frame, 0, weight=1)
        w = Label(frame, text="GLOBAL COMMANDS", font=btnfont3, fg='red', bg='gray30')
        w.grid(row=0, column=0, sticky=N + S + E + W, columnspan=2)


        # All Lamps On
        btn11 = Button(frame, text="ALL LAMPS ON", font=btnfont1, fg='OrangeRed2', bg='gray24', command = AllLampsOn())
        btn11.grid(row=1, column=0, sticky=N + S + E + W)

        # All Lamps Off
        btn12 = Button(frame, text="ALL LAMPS OFF", font=btnfont1, fg='OrangeRed2', bg='gray24', command = AllLampsOff())
        btn12.grid(row=1, column=1, sticky=N + S + E + W)

        # All Lamps Off
        btn21 = Button(frame, text="ALL ROLLERS UP", font=btnfont1, fg='OrangeRed2', bg='gray24')
        btn21.grid(row=2, column=0, sticky=N + S + E + W)

        # All Lamps Off
        btn22 = Button(frame, text="ALL ROLLERS DOWN", font=btnfont1, fg='OrangeRed2', bg='gray24')
        btn22.grid(row=2, column=1, sticky=N + S + E + W)

        # All Lamps Off
        btn31 = Button(frame, text="ALL AC ON", font=btnfont1, fg='OrangeRed2', bg='gray24')
        btn31.grid(row=3, column=0, sticky=N + S + E + W)

        # All Lamps Off
        btn32 = Button(frame, text="ALL AC OFF", font=btnfont1, fg='OrangeRed2', bg='gray24')
        btn32.grid(row=3, column=1, sticky=N + S + E + W)

        Grid.rowconfigure(frame, 4, weight=1)
        btn_main = Button(frame, text="MAIN MENU", font=btnfont1, fg='OrangeRed2', bg='gray30',
                          command=lambda: controller.show_frame("MainMenu"))
        btn_main.grid(row=4, column=0, sticky=N + S + E + W, columnspan=2)

if __name__ == "__main__":
    app = SampleApp()
    app.setFullScreen()
    app.mainloop()

