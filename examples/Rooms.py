
class Rooms(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        Grid.rowconfigure(self, 0, weight=1)
        Grid.columnconfigure(self, 0, weight=1)

        # Create & Configure frame
        frame = Frame(self)
        frame.grid(row=0, column=0, sticky=N + S + E + W)

        # Create a 3x3 (rows x columns) grid of buttons inside the frame

        for row_index in range(3):
            Grid.rowconfigure(frame, row_index, weight=2)
            for col_index in range(2):
                Grid.columnconfigure(frame, col_index, weight=2)

        btnfont1 = tkfont.Font(family='Courier', size=12, weight='bold')
        btnfont2 = font.Font(family='Courier', size=14, weight='normal')
        btnfont3 = font.Font(family='Calibri', size=18, weight='bold')

        # Label

        Grid.rowconfigure(frame, 0, weight=1)
        w = Label(frame, text="ROOMS", font=btnfont3, fg='blue', bg='gray30')

        w.grid(row=0, column=0, sticky=N + S + E + W, columnspan=2)


        # LIVING ROOM
        btn11 = Button(frame, text="LIVING ROOM", font=btnfont1, fg='OrangeRed2', bg='gray24')
        btn11.grid(row=1, column=0, sticky=N + S + E + W)

        # BED ROOM
        btn12 = Button(frame, text="BED ROOM", font=btnfont1, fg='OrangeRed2', bg='gray24')
        btn12.grid(row=1, column=1, sticky=N + S + E + W)

        # KITCHEN
        btn21 = Button(frame, text="KITCHEN", font=btnfont1, fg='OrangeRed2', bg='gray24')
        btn21.grid(row=2, column=0, sticky=N + S + E + W)

        # GUEST ROOM
        btn22 = Button(frame, text="GUEST ROOM", font=btnfont1, fg='OrangeRed2', bg='gray24')
        btn22.grid(row=2, column=1, sticky=N + S + E + W)

        # Main
        Grid.rowconfigure(frame , 3, weight=2)
        btn_main = Button(frame, text="MAIN MENU", font=btnfont1, fg='OrangeRed2', bg='gray30',
                          command = lambda: controller.show_frame("MainMenu"))

        btn_main.grid(row=3, column=0, sticky=N + S + E + W, columnspan=2)