import tkinter as tk                # python 3
from tkinter import *
from tkinter import font


class MainMenu(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        Grid.rowconfigure(self, 0, weight=1)
        Grid.columnconfigure(self, 0, weight=1)

        # Create & Configure frame
        frame = Frame(self)
        frame.grid(row=0, column=0, sticky=N + S + E + W)

        # Create (rows x columns) grid of buttons inside the frame

        for row_index in range(3):
            Grid.columnconfigure(frame, 0, weight=1)
            Grid.rowconfigure(frame, row_index, weight=1)

        btnfont1 = font.Font(family='Courier', size=12, weight='bold')
        btnfont2 = font.Font(family='Courier', size=14, weight='normal')
        btnfont3 = font.Font(family='Courier', size=14, weight='normal')

        # GlobalCommands
        btn11 = Button(frame, text="GLOBAL COMMANDS", font=btnfont1, fg='OrangeRed2', bg='gray24',
                       command= lambda : controller.show_frame("GlobalCommands"))
        btn11.grid(row=0, column=0, sticky=N + S + E + W)

        # SCENARIOS
        btn12 = Button(frame, text="SCENARIOS", font=btnfont1, fg='OrangeRed2', bg='gray24',
                       command=lambda : controller.show_frame("Scenarios"))
        btn12.grid(row=1, column=0, sticky=N + S + E + W)

        # ROOMS
        btn21 = Button(frame, text="ROOMS", font=btnfont1, fg='OrangeRed2', bg='gray24')
        btn21.grid(row=2, column=0, sticky=N + S + E + W)



    """class FullScreenApp(self):
            def __init__(self, master, **kwargs):
                self.master = master
                pad = 0
                self._geom = '480x320+0+0'
                master.geometry("{0}x{1}+0+0".format(
                    master.winfo_screenwidth() - pad, master.winfo_screenheight() - pad))
                master.bind('<Escape>', self.toggle_geom)

            def toggle_geom(self, event):
                geom = self.master.winfo_geometry()
                print(geom, self._geom)
                self.master.geometry(self._geom)
                self._geom = geom

    app = FullScreenApp(self)"""

