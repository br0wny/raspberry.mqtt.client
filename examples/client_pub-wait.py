#!/usr/bin/python
# -*- coding: utf-8 -*-


import context  # Ensures paho is in PYTHONPATH


import paho.mqtt.client as mqtt
import Tkinter as tk
import tkMessageBox



def on_connect(mqttc, obj, flags, rc):
    print("connected with rc code: " + str(rc))


def on_message(mqttc, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))
    pass


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    print(string)


# If you want to use a specific client id, used
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.

mqttc = mqtt.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe

#mqttc.username_pw_set("nlmafkti", password="iiqtq4waxTq3")
# Uncomment to enable debug messages
# mqttc.on_log = on_log

mqttc.username_pw_set("nlmafkti","iiqtq4waxTq3")
mqttc.connect("34.200.51.91", 13022, 60)
mqttc.loop_start()


class FullScreenApp(object):
    def __init__(self, master, **kwargs):
        self.master=master
        pad=3
        self._geom='200x200+0+0'
        master.geometry("{0}x{1}+0+0".format(
            master.winfo_screenwidth()-pad, master.winfo_screenheight()-pad))
        master.bind('<Escape>',self.toggle_geom)
    def toggle_geom(self,event):
        geom=self.master.winfo_geometry()
        print(geom,self._geom)
        self.master.geometry(self._geom)
        self._geom=geom

root=tk.Tk()
app=FullScreenApp(root)


def AllLampsOn():

    mqttc.publish("Test", "lamps_On")

def AllLampsOff():

    mqttc.publish("Test", "lamps_Off")

def AllRollersUp():

    mqttc.publish("Test", "Rollers_Up")

def AllRollersDown():

    mqttc.publish("Test", "Rollers_Down")

def Away():

    mqttc.publish("Test", "Away")

B1 = tk.Button(root, text = "All Lamps On", command = AllLampsOn)
B1.grid(row=0, column= 0)
B2 = tk.Button(root, text = "All Lamps Off", command = AllLampsOff)
B2.grid(row=0, column= 1)
B3 = tk.Button(root, text = "All Rollers Up", command = AllRollersUp)
B3.grid(row=1, column= 0)
B4 = tk.Button(root, text = "All Rollers Down", command = AllRollersDown)
B4.grid(row=1, column= 1)
B5 = tk.Button(root, text = "Away", command = Away)
B5.grid(row=2, column= 0)




root.mainloop()

print("tuple")
(rc, mid) = mqttc.publish("tuple", "bar", qos=2)
print("class")
infot = mqttc.publish("class", "bar", qos=2)

infot.wait_for_publish()

